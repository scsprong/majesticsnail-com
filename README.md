Basically, Bootstrap, JQuery, and FontAwesome are put in their own folders, if neccessary, but the locations are merged, if making sense.

bootstrap/css => css
bootstrap/fonts => fonts
bootstrap/js => js/bootstrap
fontawseome/css => css
fontawsome/js => js/fontawesome
fontawsome/webfonts => fonts
jquery/js => js/jquery
